package com.ucb.framework

import com.ucb.data.IRemoteDataSource
import com.ucb.domain.*


class AllpaqaDataSource(val apiRest: RetrofitBuilder): IRemoteDataSource {
    override suspend fun getAllVerbs(): List<Verb> {
        TODO("Not yet implemented")
    }

    override suspend fun getExamplesForOneVerb(): List<VerbContent> {
        TODO("Not yet implemented")
    }

    override suspend fun loginUser(userPost: UserPost): UserLogin {
        return apiRest.apiService.loginUser(userPost);
    }

    override suspend fun getAllStories(): List<Story> {
        return apiRest.apiService.getAllTales();
    }

    override suspend fun getAllCategories(): List<Category> {
        return apiRest.apiService.getAllCategories();
    }

    override suspend fun getAllCategoriesByType(type: String): List<Category> {
        return apiRest.apiService.getAllCategoriesByType(type);
    }

    override suspend fun getAllExamplesByCategoryId(id:String): List<CategoryContent> {
        return  apiRest.apiService.getAllExamplesByCategoryId(id);
    }

    override suspend fun createNewUser(user: User): User {
        return apiRest.apiService.createNewUser(user)
    }

}