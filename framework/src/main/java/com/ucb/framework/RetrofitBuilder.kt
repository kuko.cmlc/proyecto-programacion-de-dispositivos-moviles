package com.ucb.framework
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    //private const val BASE_URL = "http://10.0.2.2:8000/api/"
    private const val BASE_URL = "https://allpaqa-api.herokuapp.com/api/"
    var client: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(1000, java.util.concurrent.TimeUnit.SECONDS)
        .readTimeout(1000, java.util.concurrent.TimeUnit.SECONDS).build()

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}