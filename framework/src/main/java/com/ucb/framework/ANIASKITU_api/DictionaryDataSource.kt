package com.ucb.framework.ANIASKITU_api

import com.ucb.data.IRemoteDataSourceAniaskitu
import com.ucb.domain.*


class DictionaryDataSource(val apiRest: RetrofitBuilderAniaskitu): IRemoteDataSourceAniaskitu {
    override suspend fun getWords(): List<Dictionary> {
        return apiRest.apiService.getLastWordsDictionary();
    }

    override suspend fun searchWord(WordToSearch: SearchModel): List<Dictionary> {
        return apiRest.apiService.searchWord(WordToSearch);
    }
}