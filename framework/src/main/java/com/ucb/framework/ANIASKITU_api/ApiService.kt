package com.ucb.framework.ANIASKITU_api

import com.ucb.domain.Dictionary
import com.ucb.domain.SearchModel
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
interface ApiService {
    @GET("dictionary")
    suspend fun getLastWordsDictionary():List<Dictionary>

    @POST("dictionary/search")
    suspend fun searchWord(@Body searchBody: SearchModel):List<Dictionary>
}
