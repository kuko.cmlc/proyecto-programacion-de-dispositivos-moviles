package com.ucb.framework.server

import com.google.gson.annotations.SerializedName

data class Verb(
    var id: Number, @SerializedName("id")
    var verbQuechua: String,
    var verbSpanish: String,
    var imageURL: String,
    var time: String
)
