package com.ucb.framework

import com.ucb.domain.Category

class CategoriesResponse(val results: List<Category>)
