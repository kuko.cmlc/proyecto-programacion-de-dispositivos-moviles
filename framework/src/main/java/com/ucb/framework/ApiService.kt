package com.ucb.framework

import com.ucb.domain.Story
import com.ucb.domain.UserLogin
import com.ucb.domain.UserPost
import com.ucb.domain.Category
import com.ucb.domain.CategoryContent
import com.ucb.domain.User
import retrofit2.http.*

interface ApiService {
    @GET("tales")
    suspend fun getAllTales(): List<Story>

    @POST("login")
    suspend fun loginUser(@Body userPost: UserPost): UserLogin
    @GET("categories")
    suspend fun getAllCategories(): List<Category>

    @GET("categories/{type}")
    suspend fun getAllCategoriesByType(@Path("type") type:String): List<Category>

    @GET("examples/{id}")
    suspend fun getAllExamplesByCategoryId(@Path("id") id:String): List<CategoryContent>

    //POST USER
    @POST("user")
    suspend fun createNewUser(@Body user: User): User
}
