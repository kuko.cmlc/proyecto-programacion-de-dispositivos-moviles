package com.ucb.usecases

import com.ucb.data.CategoryRepository
import com.ucb.domain.Category
import com.ucb.domain.CategoryContent

class getAllCategoriesByType (val repository: CategoryRepository) {
    suspend fun invoke(type: String): List<Category> {
        return repository.getAllCategoriesByType(type)
    }
}