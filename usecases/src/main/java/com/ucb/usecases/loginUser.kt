package com.ucb.usecases

import com.ucb.data.UserRepository
import com.ucb.domain.UserLogin
import com.ucb.domain.UserPost

class loginUser (val repository: UserRepository) {
    suspend fun invoke(userPost: UserPost): UserLogin {
        return repository.loginUser(userPost)
    }
}