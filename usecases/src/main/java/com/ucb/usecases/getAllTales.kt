package com.ucb.usecases

import com.ucb.data.StoryRepository
import com.ucb.domain.Story

class getAllTales (val repository: StoryRepository) {
    suspend fun invoke(): List<Story> {
        return repository.getAllTales()
    }
}