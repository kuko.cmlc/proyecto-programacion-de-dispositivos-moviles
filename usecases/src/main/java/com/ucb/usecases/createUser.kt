package com.ucb.usecases

import com.ucb.data.UserRepository
import com.ucb.domain.User

class createUser (val repository: UserRepository) {
    suspend fun invokeCreateUser(user: User): User {
        return repository.createNewUser(user);
    }
}