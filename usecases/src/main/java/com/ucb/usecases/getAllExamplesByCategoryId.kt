package com.ucb.usecases

import com.ucb.data.CategoryRepository
import com.ucb.domain.Category
import com.ucb.domain.CategoryContent

class getAllExamplesByCategoryId (val repository: CategoryRepository) {
    suspend fun invoke(id: String): List<CategoryContent> {
        return repository.getAllExamplesByCategory(id);
    }
}