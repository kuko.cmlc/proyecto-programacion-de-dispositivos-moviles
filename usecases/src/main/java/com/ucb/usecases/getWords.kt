package com.ucb.usecases

import com.ucb.data.DictionaryRepository
import com.ucb.domain.Dictionary
import com.ucb.domain.SearchModel

class getWords (val repository: DictionaryRepository) {
    suspend fun invokeGetWords(): List<Dictionary> {
        return repository.getListWords()
    }
    suspend fun invokeSearchWords(wordToSearch : SearchModel): List<Dictionary> {
        return repository.searchWord(wordToSearch)
    }
}