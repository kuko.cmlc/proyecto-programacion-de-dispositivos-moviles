package com.ucb.data

import com.ucb.domain.*

interface IRemoteDataSource {
    suspend fun getAllVerbs(): List<Verb>
    suspend fun getExamplesForOneVerb(): List<VerbContent>

    //USER
    suspend fun loginUser(userPost: UserPost): UserLogin

    //STORY / TALES
    suspend fun getAllStories(): List<Story>

    //CATEGORY
    suspend fun getAllCategories(): List<Category>
    suspend fun getAllCategoriesByType(type: String): List<Category>

    // EXAMPLE CATEGORIES
    suspend fun getAllExamplesByCategoryId(id: String): List<CategoryContent>

    //POST USER
    suspend fun createNewUser(user: User): User
}