package com.ucb.data

import com.ucb.domain.*

class DictionaryRepository (val remoteDataSource: IRemoteDataSourceAniaskitu ) {
    suspend fun getListWords(): List<Dictionary>  = remoteDataSource.getWords();
    suspend fun searchWord(wordToSearch: SearchModel): List<Dictionary>  = remoteDataSource.searchWord(wordToSearch)
}