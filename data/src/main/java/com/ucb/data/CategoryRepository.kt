package com.ucb.data

import com.ucb.domain.Category
import com.ucb.domain.CategoryContent

class CategoryRepository (val remoteDataSource: IRemoteDataSource) {
    suspend fun getAllCategories(): List<Category> {
        return remoteDataSource.getAllCategories();
    }

    suspend fun getAllCategoriesByType(type: String): List<Category> {
        return remoteDataSource.getAllCategoriesByType(type);
    }

    suspend fun getAllExamplesByCategory(id : String): List<CategoryContent>{
        return remoteDataSource.getAllExamplesByCategoryId(id)
    }
}