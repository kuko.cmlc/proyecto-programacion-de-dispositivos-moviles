package com.ucb.data

import com.ucb.domain.Story
import com.ucb.domain.Verb
import com.ucb.domain.VerbContent

class VerbRepository (val remoteDataSource: IRemoteDataSource) {
    suspend fun getListOfVerbs(): List<Verb>  = remoteDataSource.getAllVerbs()
    suspend fun getConjugationsByVerb(): List<VerbContent>  = remoteDataSource.getExamplesForOneVerb()
}