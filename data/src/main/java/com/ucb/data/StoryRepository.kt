package com.ucb.data

import com.ucb.domain.Story

class StoryRepository (val remoteDataSource: IRemoteDataSource) {
    suspend fun getAllTales(): List<Story> {
        return remoteDataSource.getAllStories();
    }
}