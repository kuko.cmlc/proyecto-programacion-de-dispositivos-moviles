package com.ucb.data

import com.ucb.domain.UserLogin
import com.ucb.domain.UserPost

import com.ucb.domain.Story
import com.ucb.domain.User
import com.ucb.domain.Verb
import com.ucb.domain.VerbContent
class UserRepository (val remoteDataSource: IRemoteDataSource) {
    suspend fun loginUser(userPost: UserPost): UserLogin {
        return remoteDataSource.loginUser(userPost);
    }
    suspend fun createNewUser(user: User): User  = remoteDataSource.createNewUser(user);
}