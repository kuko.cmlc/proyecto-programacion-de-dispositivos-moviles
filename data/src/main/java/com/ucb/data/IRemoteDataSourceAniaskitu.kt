package com.ucb.data

import com.ucb.domain.*

interface IRemoteDataSourceAniaskitu {
    suspend fun getWords(): List<Dictionary>
    suspend fun searchWord(WordToSearch: SearchModel): List<Dictionary>
}