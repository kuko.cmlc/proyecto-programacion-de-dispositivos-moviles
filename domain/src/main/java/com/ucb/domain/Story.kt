package com.ucb.domain

data class Story(
    var id: Number,
    var titleQuechua: String,
    var titleSpanish: String,
    var author: String,
    var contentQuechua: String,
    var contentSpanish: String,
    var imageURL: String,
    var type: String,
    var section: String // NO IMPORTANTE
    )