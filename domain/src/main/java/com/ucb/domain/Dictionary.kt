package com.ucb.domain

class Dictionary (
    var id: String,
    var quechuaWord: String,
    var spanishWord: String,
    var description: String,
    var location: String,
    var imageUrl: String,
)