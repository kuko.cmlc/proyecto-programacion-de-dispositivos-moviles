package com.ucb.domain.uiDomain

data class Button(
    var key: Number,
    var textQuechua: String,
    var textSpanish: String,
)