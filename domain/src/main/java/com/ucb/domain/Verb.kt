package com.ucb.domain

data class Verb (
    var id: Number,
    var verbQuechua: String,
    var verbSpanish: String,
    var imageURL: String,
    var time: String
    )