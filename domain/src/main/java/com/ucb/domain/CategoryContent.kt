package com.ucb.domain

data class CategoryContent (
    var id: Number,
    var categoryId: Number,
    var sentenceQuechua: String,
    var sentenceSpanish: String,
    )