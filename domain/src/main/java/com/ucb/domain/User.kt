package com.ucb.domain

data class User(
    var name: String,
    var lastName: String,
    var password: String,
    var email: String,
)