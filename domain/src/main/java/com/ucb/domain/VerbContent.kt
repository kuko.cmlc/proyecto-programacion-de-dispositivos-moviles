package com.ucb.domain

data class VerbContent (
    var id: Number,
    var verbId: Number,
    var sentenceQuechua: String,
    var sentenceSpanish: String,
    var time: String
    )