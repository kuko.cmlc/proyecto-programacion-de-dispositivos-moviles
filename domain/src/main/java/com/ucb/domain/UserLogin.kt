package com.ucb.domain

data class UserLogin(
    var fullName: String,
    var email: String,
    var isLogin: Boolean
)