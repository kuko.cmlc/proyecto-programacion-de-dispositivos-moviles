package com.ucb.domain

data class Category(
    var id: Number,
    var categoryQuechua: String,
    var categorySpanish: String,
    )