package com.ucb.domain

data class UserPost(
    var password: String,
    var email: String
)