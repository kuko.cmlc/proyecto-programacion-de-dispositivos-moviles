package com.ucb.allpaqa.fileMockData

import com.ucb.domain.CategoryContent
import com.ucb.domain.Story
import com.ucb.domain.uiDomain.Button


class mockData {
    fun getFakeDataStory(): List<Story>{
        val lista = arrayListOf<Story>()
        lista.add(Story(1,
            "Surumanta kuntrumanta (El zorro y el cóndor)",
            "Spanish",
            "none",
            "none",
            "Spanish - NONE",
            "https://conceptodefinicion.de/wp-content/uploads/2017/11/Zorro.jpg",
            "none",
            "none"))
        lista.add(Story(2,
            "Atuq antuñumanta (Del zorro pasante)",
            "Spanish",
            "none",
            "none",
            "Spanish - NONE",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Vulpes_vulpes_2.jpg/1200px-Vulpes_vulpes_2.jpg",
            "none",
            "none"))
        lista.add(Story(3,
            "Allquwan atuxwan (Del perro y el zorro)",
            "Spanish",
            "none",
            "none",
            "Spanish - NONE",
            "https://cr00.epimg.net/programa/imagenes/2018/11/17/al_campo/1542409593_567981_1542409698_noticia_normal.jpg",
            "none",
            "none"))
        lista.add(Story(4,
            "Jukumarita (Del oso)",
            "Spanish",
            "none",
            "none",
            "Spanish - NONE",
            "https://static.eldeber.com.bo/Files/Sizes/2020/10/9/las-llamas-afectan-al-hbitat-del-jucumari_222123207_1140x520.jpg",
            "none",
            "none"))
        return lista
    }
    fun getButtonsPronouns(): List<Button>{
        val lista = arrayListOf<Button>()
        lista.add(Button(1,
            "None",
            "Yo",
        ))
        lista.add(Button(2,
            "None",
            "Tu",
        ))
        lista.add(Button(3,
            "None",
            "El",
        ))
        lista.add(Button(4,
            "None",
            "Ella",
        ))
        lista.add(Button(5,
            "None",
            "Eso",
        ))
        lista.add(Button(6,
            "None",
            "Nosotros",
        ))
        lista.add(Button(7,
            "None",
            "Ustedes",
        ))
        lista.add(Button(7,
            "None",
            "Ellos",
        ))
        return lista;
    }
    fun getPronounsExample(): List<CategoryContent>{
        val lista = arrayListOf<CategoryContent>()
        lista.add(CategoryContent(1,
                1,
                "Example quechua sentence 1",
                "Ejemplo español 1"
        ))
        lista.add(CategoryContent(2,
                2,
                "Example quechua sentence 1",
                "Ejemplo español 1"
        ))
        lista.add(CategoryContent(3,
                3,
                "Example quechua sentence 1",
                "Ejemplo español 1"
        ))
        lista.add(CategoryContent(4,
                4,
                "Example quechua sentence 1",
                "Ejemplo español 1"
        ))
        return lista;
    }
    fun getButtonsModals(): List<Button>{
        val lista = arrayListOf<Button>()
        lista.add(Button(1,
                "None",
                "Gracias",
        ))
        lista.add(Button(2,
                "None",
                "Por favor",
        ))
        lista.add(Button(3,
                "None",
                "De nada",
        ))
        return lista;
    }
    fun getButtonsRegards(): List<Button>{
        val lista = arrayListOf<Button>()
        lista.add(Button(1,
                "None",
                "Buenos dias",
        ))
        lista.add(Button(2,
                "None",
                "Buenas tardes",
        ))
        lista.add(Button(3,
                "None",
                "Buenas noches",
        ))
        return lista;
    }
}