package com.ucb.allpaqa

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.ucb.allpaqa.adapters.custom_dialog
import com.ucb.allpaqa.userLogin.UserSessionManager
import com.ucb.allpaqa.viewModels.UserViewModel
import com.ucb.data.UserRepository
import com.ucb.domain.User
import com.ucb.framework.RetrofitBuilder
import com.ucb.framework.AllpaqaDataSource
import com.ucb.usecases.createUser
import kotlinx.android.synthetic.main.activity_register_page.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class register_page : AppCompatActivity() {
    private var userViewModel = UserViewModel(
        createUser(
            UserRepository(
                AllpaqaDataSource(
                    RetrofitBuilder
                )
            )
        )
    )
    var confirmPassword = ""
    var password = ""
    var email = ""
    var name = ""
    var lastName = ""
    private lateinit var session: UserSessionManager;
    val loading = custom_dialog(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_page)
        btn_register_by_google.isVisible = false
        session = UserSessionManager(getApplicationContext())
        if (session.isUserLoggedIn){
            var login_activity = Intent(this,menu_stories_list_page::class.java);
            startActivity(login_activity);
        }
        userViewModel.model.observe(this, Observer(::checkPostSuccessFull))
        btn_register_user.setOnClickListener(){
            getDataFromForms();
            if (ValidateNotEmptyInputs()){
                loading.startLoading()
                val userToPost = User(name,lastName,password,email)
                userViewModel.createNewUser(userToPost)
            }
        }
    }

    private fun checkPostSuccessFull(uiModel: UserViewModel.UiModel) {
        when (uiModel) {
            is UserViewModel.UiModel.Content -> postUser(uiModel.user)
        }
    }

    fun postUser(user: User){
        loading.isDismiss()
        if (user.email == email){
            var login_page = Intent(this, login_page::class.java)
            startActivity(login_page);
        }
        else{
            Toast.makeText(this, R.string.user_is_registered, Toast.LENGTH_SHORT).show()
        }
    }

    fun isEmailValid(email: CharSequence?): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern: Pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun getDataFromForms(){
        confirmPassword = editTextConfirmPassword.text.toString().trim()
        password = editTextPassword.text.toString().trim()
        email = editTextEmailAddress.text.toString().trim()
        name = editTextName.text.toString().trim()
        lastName = editTextLastName.text.toString().trim()
    }

    fun ValidateNotEmptyInputs() : Boolean{
        var result = true;
        if (email.isEmpty() || name.isEmpty() || lastName.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()){
            Toast.makeText(this, R.string.form_Error, Toast.LENGTH_SHORT).show()
            result = false;
        }
        else {
            if (!isEmailValid(email)){
                Toast.makeText(this, R.string.email_Error   , Toast.LENGTH_SHORT).show()
                result = false
            }
            else {
                if (password != confirmPassword){
                    Toast.makeText(this, R.string.password_Error, Toast.LENGTH_SHORT).show()
                    result = false
                }
            }
        }
        return result;
    }
}
