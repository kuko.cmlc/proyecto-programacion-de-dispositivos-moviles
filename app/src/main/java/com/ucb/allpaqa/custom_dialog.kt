package com.ucb.allpaqa.adapters

import android.app.Activity
import android.app.AlertDialog
import com.ucb.allpaqa.R

class custom_dialog (val mActivity: Activity){
    private lateinit var isdialog: AlertDialog
    fun startLoading(){
        val infalter = mActivity.layoutInflater
        val dialogView = infalter.inflate(R.layout.custom_dialog,null)
        val bulider = AlertDialog.Builder(mActivity)
        bulider.setView(dialogView)
        bulider.setCancelable(false)
        isdialog = bulider.create()
        isdialog.show()
    }
    fun isDismiss(){
        if (isdialog.isShowing){
            isdialog.dismiss()
        }
    }
}