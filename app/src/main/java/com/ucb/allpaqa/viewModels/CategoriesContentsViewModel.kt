package com.ucb.allpaqa.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ucb.data.CategoryRepository
import com.ucb.domain.Category
import com.ucb.domain.CategoryContent
import com.ucb.usecases.getAllCategoriesByType
import kotlinx.coroutines.launch

class CategoriesContentsViewModel(private val getAllCategoriesByType: getAllCategoriesByType) : ScopedViewModel() {
    private val _model = MutableLiveData<UiModel>()

    val model: LiveData<UiModel>
        get() = _model

    sealed class UiModel {
        class Content(val category: List<Category>) : UiModel()
    }

    fun getAllCategoriesByType(type: String) {
        viewModelScope.launch {
            _model.value = UiModel.Content(getAllCategoriesByType.invoke(type));
        }
    }

}