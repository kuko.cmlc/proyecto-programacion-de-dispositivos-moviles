package com.ucb.allpaqa.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ucb.domain.Category
import com.ucb.domain.CategoryContent
import com.ucb.usecases.getAllCategoriesByType
import com.ucb.usecases.getAllExamplesByCategoryId
import kotlinx.coroutines.launch

class CategoriesExampleViewModel(private val getAllExamplesByCategoryId: getAllExamplesByCategoryId) : ScopedViewModel() {
    private val _model = MutableLiveData<UiModel>()

    val model: LiveData<UiModel>
        get() = _model

    sealed class UiModel {
        class Content(val category: List<CategoryContent>) : UiModel()
    }

    fun getAllExamplesByCategoryId(id: String) {
        viewModelScope.launch {
            _model.value = UiModel.Content(getAllExamplesByCategoryId.invoke(id));
        }
    }

}