package com.ucb.allpaqa.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ucb.data.UserRepository
import com.ucb.domain.UserLogin
import com.ucb.domain.UserPost
import com.ucb.usecases.loginUser
import kotlinx.coroutines.launch

class UserLoginViewModel(private val loginUser: loginUser) : ScopedViewModel() {
    private val _model = MutableLiveData<UiModel>()

    val model: LiveData<UiModel>
        get() = _model

    sealed class UiModel {
        class Content(val user: UserLogin) : UiModel()
    }

    fun loginUser(userPost: UserPost) {
        viewModelScope.launch {
            _model.value = UiModel.Content(loginUser.invoke(userPost));
        }
    }
}