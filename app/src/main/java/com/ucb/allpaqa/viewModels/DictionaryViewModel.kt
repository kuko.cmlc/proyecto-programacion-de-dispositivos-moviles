package com.ucb.allpaqa.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ucb.data.StoryRepository
import com.ucb.domain.Dictionary
import com.ucb.domain.SearchModel
import com.ucb.domain.Story
import com.ucb.usecases.getAllTales
import com.ucb.usecases.getWords
import kotlinx.coroutines.launch

class DictionaryViewModel(private val getWords: getWords) : ScopedViewModel() {
    private val _model = MutableLiveData<UiModel>()

    val model: LiveData<UiModel>
        get() = _model

    sealed class UiModel {
        class Content(val words: List<Dictionary>) : UiModel()
    }

    fun getAllWords() {
        viewModelScope.launch {
            _model.value = UiModel.Content(getWords.invokeGetWords());
        }
    }

    fun searchWord(wordToSearch: SearchModel) {
        viewModelScope.launch {
            _model.value = UiModel.Content(getWords.invokeSearchWords(wordToSearch));
        }
    }
}