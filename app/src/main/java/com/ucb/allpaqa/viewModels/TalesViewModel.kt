package com.ucb.allpaqa.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ucb.data.StoryRepository
import com.ucb.domain.Story
import com.ucb.usecases.getAllTales
import kotlinx.coroutines.launch

class TalesViewModel(private val getAllTales: getAllTales) : ScopedViewModel() {
    private val _model = MutableLiveData<UiModel>()

    val model: LiveData<UiModel>
        get() = _model

    sealed class UiModel {
        class Content(val tales: List<Story>) : UiModel()
    }

    fun getAllTales() {
        viewModelScope.launch {
            _model.value = UiModel.Content(getAllTales.invoke());
        }
    }

}