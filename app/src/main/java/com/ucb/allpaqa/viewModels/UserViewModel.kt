package com.ucb.allpaqa.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ucb.data.UserRepository
import com.ucb.domain.User
import com.ucb.domain.UserLogin
import com.ucb.domain.UserPost
import com.ucb.usecases.createUser
import com.ucb.usecases.loginUser
import kotlinx.coroutines.launch

class UserViewModel(private val createUser: createUser) : ScopedViewModel() {
    private val _model = MutableLiveData<UiModel>()

    val model: LiveData<UiModel>
        get() = _model

    sealed class UiModel {
        class Content(val user: User) : UiModel()
    }

    fun createNewUser(user: User) {
        viewModelScope.launch {
            _model.value = UiModel.Content(createUser.invokeCreateUser(user));
        }
    }

}