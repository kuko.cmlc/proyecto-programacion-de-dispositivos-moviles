package com.ucb.allpaqa.adapters.story

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.R
import com.ucb.allpaqa.story_page
import com.ucb.domain.Story
import kotlinx.android.synthetic.main.row_list.view.*

class Story_adapter (val list: List<Story>, val context: Context): RecyclerView.Adapter<Story_adapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItems(model: Story) {
            itemView.txtName.text = model.titleQuechua;
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_list,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list.get(position))
        holder.itemView.txtName.setOnClickListener(){
            val story = list.get(position);
            var storyPage = Intent(context, story_page::class.java)
            storyPage.putExtra("titleQuechua",story.titleQuechua)
            storyPage.putExtra("titleSpanish",story.titleSpanish)
            storyPage.putExtra("contentQuechua",story.contentQuechua)
            storyPage.putExtra("contentSpanish",story.contentSpanish)
            storyPage.putExtra("imageURL",story.imageURL)
            storyPage.putExtra("author",story.author)
            storyPage.putExtra("type",story.type)
            storyPage.putExtra("section",story.section)
            context.startActivity(storyPage)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}