package com.ucb.allpaqa.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.R
import com.ucb.domain.CategoryContent
import kotlinx.android.synthetic.main.row_content.view.*

class content_adapter (val list: List<CategoryContent>, val context: Context): RecyclerView.Adapter<content_adapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItems(model: CategoryContent) {
            itemView.content_quechua.text = model.sentenceQuechua;
            itemView.content_spanish.text = model.sentenceSpanish;
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_content,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}