package com.ucb.allpaqa.adapters.story

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.ucb.allpaqa.R
import com.ucb.allpaqa.story_page
import com.ucb.domain.Story
import kotlinx.android.synthetic.main.row_list_story_view_2.view.*

class Story_adapter_view_2 (val list: List<Story>, val context: Context): RecyclerView.Adapter<Story_adapter_view_2.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItems(model: Story) {
            Picasso.get().load(model.imageURL).error(R.drawable.allpaqa_3).into(itemView.imageTale);
            itemView.txtTitleQuechua.text = model.titleQuechua;
            itemView.txtTitleSpanish.text = model.titleSpanish;
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_list_story_view_2,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list.get(position))
        holder.itemView.imageTale.setOnClickListener(){
            val story = list.get(position);
            var storyPage = Intent(context, story_page::class.java)
            storyPage.putExtra("titleQuechua",story.titleQuechua)
            storyPage.putExtra("titleSpanish",story.titleSpanish)
            storyPage.putExtra("contentQuechua",story.contentQuechua)
            storyPage.putExtra("contentSpanish",story.contentSpanish)
            storyPage.putExtra("imageURL",story.imageURL)
            storyPage.putExtra("author",story.author)
            storyPage.putExtra("type",story.type)
            storyPage.putExtra("section",story.section)
            context.startActivity(storyPage)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}