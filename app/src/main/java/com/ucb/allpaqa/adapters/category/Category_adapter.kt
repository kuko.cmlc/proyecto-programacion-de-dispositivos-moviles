package com.ucb.allpaqa.adapters.category

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.R
import com.ucb.domain.Category
import com.ucb.domain.CategoryContent
import kotlinx.android.synthetic.main.activity_word.*
import kotlinx.android.synthetic.main.row_content.view.*

class Category_adapter (val list: List<CategoryContent>, val context: Context): RecyclerView.Adapter<Category_adapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItems(model: CategoryContent) {
            itemView.content_quechua.text =
                model.sentenceSpanish.substring(0, 1).toUpperCase() + model.sentenceSpanish.substring(1);
            itemView.content_spanish.text =
                model.sentenceQuechua.substring(0, 1).toUpperCase() + model.sentenceQuechua.substring(1);
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_content,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}