package com.ucb.allpaqa.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.R
import com.ucb.allpaqa.intro_slide

class intro_slider_adapter (private val introSlides: List<intro_slide>)
    : RecyclerView.Adapter<intro_slider_adapter.IntroSlideViewHolder>() {
    inner class IntroSlideViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val textTitle = view.findViewById<TextView>(R.id.text_title)
        private val textDescription = view.findViewById<TextView>(R.id.text_description)
        private val imageIcon = view.findViewById<ImageView>(R.id.image_slide_icon)

        fun bind(introSlide: intro_slide){
            textTitle.text = introSlide.title;
            textDescription.text = introSlide.description;
            imageIcon.setImageResource(introSlide.icon);
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroSlideViewHolder {
        return IntroSlideViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.slide_item_container,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return introSlides.size
    }

    override fun onBindViewHolder(holder: IntroSlideViewHolder, position: Int) {
        holder.bind(introSlides[position])
    }
}
