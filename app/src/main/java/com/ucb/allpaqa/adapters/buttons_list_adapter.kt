package com.ucb.allpaqa.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.R
import com.ucb.domain.uiDomain.Button
import kotlinx.android.synthetic.main.row_buttons.view.*

class buttons_list_adapter (val list: List<Button>, val context: Context): RecyclerView.Adapter<buttons_list_adapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItems(model: Button) {
            itemView.btn_option.text = model.textSpanish;
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_buttons,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}