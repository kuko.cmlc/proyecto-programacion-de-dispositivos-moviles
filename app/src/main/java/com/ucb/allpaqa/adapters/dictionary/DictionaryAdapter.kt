package com.ucb.allpaqa.adapters.dictionary

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.R
import com.ucb.allpaqa.wordActivity
import com.ucb.domain.Dictionary
import kotlinx.android.synthetic.main.row_buttons.view.*
import kotlinx.android.synthetic.main.row_word.view.*

class DictionaryAdapter(val list: List<Dictionary>, val Lenguage: String, val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var LENGUAGE = Lenguage
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_word, parent, false)
        return MainViewHoldel(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val word = list.get(position)
        if (LENGUAGE == "QUECHUA") {
            holder.itemView.findViewById<TextView>(R.id.btn_word_quechua).text = word.quechuaWord
        } else {
            holder.itemView.findViewById<TextView>(R.id.btn_word_quechua).text = word.spanishWord
        }
        holder.itemView.btn_word_quechua.setOnClickListener() {
            var storyPage = Intent(context, wordActivity::class.java)
            storyPage.putExtra("description", word.description)
            storyPage.putExtra("spanishWord", word.spanishWord)
            storyPage.putExtra("quechuaWord", word.quechuaWord)
            if (word.imageUrl.isEmpty()) {
                storyPage.putExtra("imageUrl", "No Image Path")
            } else {
                storyPage.putExtra("imageUrl", word.imageUrl)
            }
            storyPage.putExtra("location", word.location)
            context.startActivity(storyPage)
        }
    }
}

class MainViewHoldel(view: View) : RecyclerView.ViewHolder(view);