package com.ucb.allpaqa

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.ucb.allpaqa.userLogin.UserSessionManager
import kotlinx.android.synthetic.main.activity_login_page.*
import androidx.lifecycle.Observer
import com.ucb.allpaqa.adapters.custom_dialog
import com.ucb.allpaqa.viewModels.UserLoginViewModel
import com.ucb.data.UserRepository
import com.ucb.domain.UserLogin
import com.ucb.domain.UserPost
import com.ucb.framework.RetrofitBuilder
import com.ucb.framework.AllpaqaDataSource
import com.ucb.usecases.loginUser
import java.util.regex.Matcher
import java.util.regex.Pattern

class login_page : AppCompatActivity() {
    private var mainviewView = UserLoginViewModel(loginUser(UserRepository(AllpaqaDataSource(
            RetrofitBuilder))))
    private lateinit var session: UserSessionManager;
    val loading = custom_dialog(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_page)
        login_button_google.isVisible = false
        session = UserSessionManager(getApplicationContext())
        if (session.isUserLoggedIn){
            var login_activity = Intent(this,menu_stories_list_page::class.java);
            startActivity(login_activity);
        }
        register_text.setOnClickListener() {
            var register_user = Intent(this, register_page::class.java)
            startActivity(register_user);
        }
        login_button.setOnClickListener() {
            val username: String = edit_email_text.getText().toString()
            val password: String = edit_password_text.getText().toString()
            if (username.trim { it <= ' ' }.isNotEmpty() && password.trim { it <= ' ' }.isNotEmpty()) {
                if (isEmailValid(username)){
                    val userPost = UserPost(password, username)
                    loading.startLoading()
                    mainviewView.model.observe(this, Observer(::updateUi))
                    mainviewView.loginUser(userPost)
                }
                else {
                    Toast.makeText(
                        applicationContext,
                        R.string.email_Error,
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    R.string.login_message_no_username_password,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun isEmailValid(email: CharSequence?): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern: Pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun updateUi(uiModel: UserLoginViewModel.UiModel) {
        when (uiModel) {
            is UserLoginViewModel.UiModel.Content -> loadUser(uiModel.user)
        }
    }

    fun loadUser(userLogin: UserLogin) {
        loading.isDismiss()
        if(userLogin.isLogin == true) {
            session.createUserLoginSession(userLogin.fullName, userLogin.email)
            var introduction_page = Intent(this, introduction_page::class.java)
            introduction_page.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            introduction_page.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(introduction_page)
            finish()
        }
        else {
            Toast.makeText(
                    applicationContext,
                    R.string.login_message_wrong_username_password,
                    Toast.LENGTH_LONG
            ).show()
        }
    }

}