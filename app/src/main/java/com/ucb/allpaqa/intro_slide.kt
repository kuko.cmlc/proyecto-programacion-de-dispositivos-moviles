package com.ucb.allpaqa

data class intro_slide (
    val title: String,
    val description: String,
    val icon: Int
)