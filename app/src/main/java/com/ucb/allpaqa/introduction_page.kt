package com.ucb.allpaqa

import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.viewpager2.widget.ViewPager2
import com.ucb.allpaqa.adapters.intro_slider_adapter
import kotlinx.android.synthetic.main.activity_introduction_page.*


class introduction_page : AppCompatActivity() {

   private val intro_slider_adapter = intro_slider_adapter(
        listOf(
            intro_slide(
                "Cuenta con un Diccionario a la mano Quechua - Español",
                "¡Con este diccionario podras buscar la pabras de quechua a español y viceversa!",
                R.drawable.initial_image1
            ),
            intro_slide(
                "Lee Cuentos en Quechua - Español sorprendentes",
                "¡Podras leer fabulas, cuentos, leyendas y mitos originarios de Bolivia u otros paises latinoamericanos!",
                R.drawable.initial_image2
            ),
            intro_slide(
                "Aprende con nosotros",
                "Aprende los fundamentos básicos del idioma Quechua Boliviano, como ser colores, pronombres y más.",
                R.drawable.initial_image3
            ),
            intro_slide(
                "Empecemos aprendiendo juntos con ALLPAQA",
                "",
                R.drawable.initial_image4
            )
        )
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_introduction_page)
        intro_slider_view_pager.adapter = intro_slider_adapter
        setupIndicator()
        setCurrentIndicator(0)
        intro_slider_view_pager.registerOnPageChangeCallback(object :
             ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentIndicator(position)
            }
        })
        button_next.setOnClickListener {
            if (intro_slider_view_pager.currentItem + 1 < intro_slider_adapter.itemCount) {
                intro_slider_view_pager.currentItem += 1
            } else {
                Intent(applicationContext, menu_stories_list_page::class.java).also {
                    startActivity(it)
                    //finish()
                }
            }
        }
        skip_intro.setOnClickListener{
            Intent(applicationContext, menu_stories_list_page::class.java).also {
                startActivity(it)
                //finish()
            }
        }
    }
    private fun setupIndicator(){
        val indicators = arrayOfNulls<ImageView>(intro_slider_adapter.itemCount)
        var layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8,0,8,0)
        for (i in indicators.indices) {
            indicators[i]= ImageView(applicationContext)
            indicators[i].apply {
                this?.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )
                this?.layoutParams = layoutParams
            }
            indicators_container.addView(indicators[i])
        }
    }
    private fun setCurrentIndicator(index: Int) {
        val chilCount = indicators_container.childCount
        for (i in 0 until chilCount) {
            val imageView = indicators_container[i] as ImageView
            if(i == index) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_active
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )
            }
        }
    }
}