package com.ucb.allpaqa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_word.*

class wordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_word)
        btn_dictionary_back.setOnClickListener() {
            var nextActivity = Intent(this, Dictionary_list::class.java);
            startActivity(nextActivity);
        }
        btn_back_dictionary.setOnClickListener(){
            var nextActivity = Intent(this, Dictionary_list::class.java);
            startActivity(nextActivity);
        }
        prepareAndDisplayData();
    }
    fun prepareAndDisplayData() {
        var spanishWord = getIntent().getStringExtra("spanishWord").toString();
        var descriptiom = getIntent().getStringExtra("description").toString();
        var quechuaWord = getIntent().getStringExtra("quechuaWord").toString();
        var imageUrl = getIntent().getStringExtra("imageUrl").toString();
        Picasso.get().load(imageUrl).error(R.drawable.allpaqa_3_removebg).into(word_image_url);
        txt_spanish_word.text =
            spanishWord.substring(0, 1).toUpperCase() + spanishWord.substring(1);
        txt_description_word.text =
            descriptiom.substring(0, 1).toUpperCase() + descriptiom.substring(1);
        txt_quechua_word.text =
            quechuaWord.substring(0, 1).toUpperCase() + quechuaWord.substring(1);
    }
}