package com.ucb.allpaqa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_story_page.*

class story_page : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_page)
        val imageURL = getIntent().getStringExtra("imageURL");
        Picasso.get().load(imageURL).into(story_image);
        title_story.text = getIntent().getStringExtra("titleQuechua");
        content_story.text = getIntent().getStringExtra("contentQuechua");
        btn_spanish.setOnClickListener(){
            content_story.text = getIntent().getStringExtra("contentSpanish");
        }
        btn_quechua.setOnClickListener(){
            content_story.text = getIntent().getStringExtra("contentQuechua");
        }
    }
}