package com.ucb.allpaqa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.ucb.allpaqa.adapters.custom_dialog
import com.ucb.allpaqa.userLogin.UserSessionManager
import kotlinx.android.synthetic.main.activity_initial_page.*

class initial_page : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial_page)
        val session = UserSessionManager(getApplicationContext());
        if (session.isUserLoggedIn){
            var login_activity = Intent(this,menu_stories_list_page::class.java);
            startActivity(login_activity);
        }
        button_login.setOnClickListener() {
            var login_activity = Intent(this, login_page::class.java);
            startActivity(login_activity);
        }
        register_text.setOnClickListener() {
            var register_user = Intent(this, register_page::class.java)
            startActivity(register_user);
        }
    }
}