package com.ucb.allpaqa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_about_the_app.*

class about_the_app : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_the_app)
        btn_back_about.setOnClickListener(){
            var dictionary_activity = Intent(this,menu_stories_list_page::class.java)
            startActivity(dictionary_activity);
        }
    }
}