package com.ucb.allpaqa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.adapters.category.Category_adapter
import com.ucb.allpaqa.userLogin.UserSessionManager
import kotlinx.android.synthetic.main.activity_view_content_learn.*
import com.ucb.allpaqa.viewModels.CategoriesContentsViewModel
import com.ucb.allpaqa.viewModels.CategoriesExampleViewModel
import com.ucb.data.CategoryRepository
import com.ucb.domain.Category
import com.ucb.domain.CategoryContent
import com.ucb.framework.RetrofitBuilder
import com.ucb.framework.AllpaqaDataSource
import com.ucb.usecases.getAllCategoriesByType
import com.ucb.usecases.getAllExamplesByCategoryId

class view_content_learn : AppCompatActivity() {
    private var title = "";
    private var type = "";
    private lateinit var session: UserSessionManager;
    private var mainviewView = CategoriesContentsViewModel(
        getAllCategoriesByType(
            CategoryRepository(
                AllpaqaDataSource(
                    RetrofitBuilder
                )
            )
        )
    )
    private var imageUrl = 0;
    private var categoryExampleViewModel = CategoriesExampleViewModel(
        getAllExamplesByCategoryId(
            CategoryRepository(
                AllpaqaDataSource(RetrofitBuilder)
            )
        )
    )
    lateinit var recyclerViewContent: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_content_learn)
        session = UserSessionManager(getApplicationContext())
        setDataToActivity()
        setUserData()

        recyclerViewContent = findViewById(R.id.recyclerView_content)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerViewContent.layoutManager = linearLayoutManager

        mainviewView.model.observe(this, Observer(::updateUi))
        mainviewView.getAllCategoriesByType(type)
        categoryExampleViewModel.model.observe(this, Observer(::updateUiExamples))
        lienarLayoutLoading.isVisible = true
    }

    private fun setDataToActivity() {
        title = getIntent().getStringExtra("title").toString()
        type = getIntent().getStringExtra("type").toString()
        imageUrl = getIntent().getStringExtra("image").toString().toInt()
        val d = resources.getDrawable(imageUrl)
        image_content_learn.setImageDrawable(d)
        text_title.text = title
    }

    private fun updateUi(uiModel: CategoriesContentsViewModel.UiModel) {
        when (uiModel) {
            is CategoriesContentsViewModel.UiModel.Content -> createButton(uiModel.category)
        }
    }

    private fun updateUiExamples(uiModel: CategoriesExampleViewModel.UiModel) {
        when (uiModel) {
            is CategoriesExampleViewModel.UiModel.Content -> loadExamples(uiModel.category)
        }
    }

    private fun setUserData() {
        val user = session.userDetails
        val name = user[UserSessionManager.KEY_NAME]
        val email = user[UserSessionManager.KEY_EMAIL]
        user_name.text = email
        welcome_text_again.text = name
        logout_text.setOnClickListener() {
            session.logoutUser();
            var initial_page = Intent(this, initial_page::class.java)
            startActivity(initial_page);
        }
    }

    private fun loadExamples(list_content: List<CategoryContent>) {
        if (!list_content.isEmpty()) {
            recyclerViewContent.adapter = Category_adapter(list_content, this)
        }
    }

    private fun isNA(sentence: String) {
        if (sentence == "N/A") {
            findViewById<TextView>(R.id.button_text_center).isVisible = false;
        } else {
            findViewById<TextView>(R.id.button_text_center).text = sentence
        }
    }

    private fun createButton(buttons: List<Category>) {
        lienarLayoutLoading.isVisible = false
        isNA(buttons[0].categoryQuechua)
        categoryExampleViewModel.getAllExamplesByCategoryId(buttons[0].id.toString());
        val layout = findViewById(R.id.list_buttons) as LinearLayout
        for (buttonMenu in buttons) {
            val button = Button(this)
            button.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                120
            )
            button.text = buttonMenu.categorySpanish
            val param = button.layoutParams as ViewGroup.MarginLayoutParams
            param.setMargins(15, 10, 15, 10)
            button.layoutParams = param
            button.setTransformationMethod(null);
            button.textSize = 16F;
            button.setBackgroundResource(R.drawable.button)
            button.setOnClickListener {
                isNA(buttonMenu.categoryQuechua)
                categoryExampleViewModel.getAllExamplesByCategoryId(buttonMenu.id.toString());
            }
            layout.addView(button)
        }
    }
}