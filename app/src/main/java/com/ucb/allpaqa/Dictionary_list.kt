package com.ucb.allpaqa

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.adapters.dictionary.DictionaryAdapter
import com.ucb.allpaqa.userLogin.UserSessionManager
import com.ucb.allpaqa.viewModels.DictionaryViewModel
import com.ucb.data.DictionaryRepository
import com.ucb.domain.Dictionary
import com.ucb.domain.SearchModel
import com.ucb.framework.ANIASKITU_api.DictionaryDataSource
import com.ucb.framework.ANIASKITU_api.RetrofitBuilderAniaskitu
import com.ucb.usecases.getWords
import kotlinx.android.synthetic.main.activity_dictionary_list.*
import kotlinx.android.synthetic.main.activity_dictionary_list.lienarLayoutLoading
import kotlinx.android.synthetic.main.activity_dictionary_list.logout_text
import kotlinx.android.synthetic.main.activity_dictionary_list.menu_option_about
import kotlinx.android.synthetic.main.activity_dictionary_list.menu_option_learn
import kotlinx.android.synthetic.main.activity_dictionary_list.menu_option_stories
import kotlinx.android.synthetic.main.activity_dictionary_list.user_name
import kotlinx.android.synthetic.main.activity_dictionary_list.welcome_text_again
import kotlin.system.exitProcess

class Dictionary_list : AppCompatActivity() {
    private lateinit var session: UserSessionManager;
    private var DEFAULT_LENGUAJE = "QUECHUA"
    lateinit var recyclerView: RecyclerView
    private var dictionaryViewModel = DictionaryViewModel(getWords(DictionaryRepository(DictionaryDataSource(RetrofitBuilderAniaskitu))))
    private var backPressedTime:Long = 0
    lateinit var backToast: Toast
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dictionary_list)
        initRecyclerView();
        session = UserSessionManager(getApplicationContext())
        setUserData();
        eventsButtons();
        lienarLayoutLoading.isVisible=true;
        recycler_word.isVisible=false;
    }

    private fun setUserData(){
        val user = session.userDetails
        val name = user[UserSessionManager.KEY_NAME]
        val email = user[UserSessionManager.KEY_EMAIL]
        user_name.text = email
        welcome_text_again.text = name
    }

    private fun initRecyclerView() {
        image_not_found.isVisible = false
        recyclerView = findViewById(R.id.recycler_word)
        val layoutManager = GridLayoutManager(this, 3)
        layoutManager.orientation = RecyclerView.VERTICAL
        recyclerView.layoutManager = layoutManager
        dictionaryViewModel.model.observe(this, Observer(::updateUi))
        dictionaryViewModel.getAllWords()
    }

    private fun updateUi(uiModel: DictionaryViewModel.UiModel) {
        when (uiModel) {
            is DictionaryViewModel.UiModel.Content -> loadWordDictionary(uiModel.words)
        }
    }

    private fun loadWordDictionary(words: List<Dictionary>) {
        lienarLayoutLoading.isVisible=false;
        recycler_word.isVisible=true;
        if (words.isEmpty()) {
            image_not_found.animation = AnimationUtils.loadAnimation(this, R.anim.word_animation)
            image_not_found.isVisible = true
            recyclerView.isVisible = false
        } else {
            image_not_found.isVisible = false
            recyclerView.isVisible = true
        }
        recyclerView.adapter = DictionaryAdapter(words, DEFAULT_LENGUAJE, this)
    }

    fun SwapLenguaje() {
        if (DEFAULT_LENGUAJE === "QUECHUA") {
            DEFAULT_LENGUAJE = "SPANISH"
            btn_text_input_1.text = getString(R.string.txt_spanish)
            btn_text_input_2.text = getString(R.string.txt_quechua)
        } else {
            DEFAULT_LENGUAJE = "QUECHUA"
            btn_text_input_1.text = getString(R.string.txt_quechua)
            btn_text_input_2.text = getString(R.string.txt_spanish)
        }
        dictionaryViewModel.getAllWords();
    }

    fun searchWord() {
        lienarLayoutLoading.isVisible=true;
        recycler_word.isVisible=false;
        var searchWord = SearchModel(
            DEFAULT_LENGUAJE,
            edit_text_search.text.toString().toLowerCase().trim()
        )
        dictionaryViewModel.searchWord(searchWord);
    }

    private fun closeKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun eventsButtons() {
        btn_swap_lenguaje.setOnClickListener() {
            SwapLenguaje()
        }
        menu_option_about.setOnClickListener(){
            var about_the_app = Intent(this, about_the_app::class.java)
            startActivity(about_the_app);
        }
        menu_option_learn.setOnClickListener(){
            var introduction_page = Intent(this, options_learns_page::class.java)
            startActivity(introduction_page);
        }
        menu_option_stories.setOnClickListener(){
            var nextActivity = Intent(this, menu_stories_list_page::class.java)
            startActivity(nextActivity);
        }
        logout_text.setOnClickListener(){
            session.logoutUser();
            var initial_page = Intent(this, initial_page::class.java)
            startActivity(initial_page);
        }
        edit_text_search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                searchWord()
                closeKeyBoard()
            }
            false
        })
    }

    override fun onBackPressed() {
        backToast = Toast.makeText(this, R.string.close_meesage, Toast.LENGTH_LONG)
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            finishAffinity();
            exitProcess(0)
        } else {
            backToast.show()
        }
        backPressedTime = System.currentTimeMillis()
    }
}