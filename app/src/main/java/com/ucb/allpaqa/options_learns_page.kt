package com.ucb.allpaqa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import com.ucb.allpaqa.userLogin.UserSessionManager
import kotlinx.android.synthetic.main.activity_options_learns_page.*
import kotlinx.android.synthetic.main.activity_options_learns_page.logout_text
import kotlinx.android.synthetic.main.activity_options_learns_page.menu_option_about
import kotlinx.android.synthetic.main.activity_options_learns_page.menu_option_dictionary
import kotlinx.android.synthetic.main.activity_options_learns_page.user_name
import kotlinx.android.synthetic.main.activity_options_learns_page.welcome_text_again
import kotlin.system.exitProcess

class options_learns_page : AppCompatActivity() {
    private lateinit var session: UserSessionManager;
    private var backPressedTime:Long = 0
    lateinit var backToast: Toast
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_options_learns_page)
        session = UserSessionManager(getApplicationContext())
        line_2.isVisible=false
        setUserData();
        optionMenuEducation();
        optionMenu();
    }

    fun optionMenuEducation(){
        btn_image_pronouns.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","pronoms")
            new_activity.putExtra("title","Pronombres")
            new_activity.putExtra("image",R.drawable.ic_passing_by_bro.toString())
            startActivity(new_activity);
        }
        btn_image_regards.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","greetings")
            new_activity.putExtra("title","Saludos")
            new_activity.putExtra("image",R.drawable.ic_hang_out_bro.toString())
            startActivity(new_activity);
        }
        btn_image_modals.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","manners")
            new_activity.putExtra("title","Modales")
            new_activity.putExtra("image",R.drawable.ic_nerd_bro.toString())
            startActivity(new_activity);
        }
        btn_image_grammar.setOnClickListener(){
            var new_activity = Intent(this, view_content_grammar::class.java)
            startActivity(new_activity);
        }
        btn_image_food.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","food")
            new_activity.putExtra("title","Comida")
            new_activity.putExtra("image",R.drawable.ic_hamburger_pana.toString())
            startActivity(new_activity);
        }
        btn_image_dates.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","dates")
            new_activity.putExtra("title","Fecha")
            new_activity.putExtra("image",R.drawable.ic_calendar_bro.toString())
            startActivity(new_activity);
        }
        btn_image_animals.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","animals")
            new_activity.putExtra("title","Animales")
            new_activity.putExtra("image",R.drawable.ic_good_doggy_bro.toString())
            startActivity(new_activity);
        }
        btn_image_colors.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","colors")
            new_activity.putExtra("title","Colores")
            new_activity.putExtra("image",R.drawable.ic_palette_bro.toString())
            startActivity(new_activity);
        }
        btn_image_numbers.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","numbers")
            new_activity.putExtra("title","Números")
            new_activity.putExtra("image",R.drawable.ic_calculator_bro.toString())
            startActivity(new_activity);
        }
        btn_image_family.setOnClickListener(){
            var new_activity = Intent(this, view_content_learn::class.java)
            new_activity.putExtra("type","family")
            new_activity.putExtra("title","Familia")
            new_activity.putExtra("image",R.drawable.ic_having_fun_bro.toString())
            startActivity(new_activity);
        }
    }

    private fun setUserData(){
        val user = session.userDetails
        val name = user[UserSessionManager.KEY_NAME]
        val email = user[UserSessionManager.KEY_EMAIL]
        user_name.text = email
        welcome_text_again.text = name
    }

    fun optionMenu(){
        menu_option_stories.setOnClickListener(){
            var nextActivity = Intent(this, menu_stories_list_page::class.java)
            startActivity(nextActivity);
        }
        menu_option_dictionary.setOnClickListener(){
            var dictionary_activity = Intent(this, Dictionary_list::class.java)
            startActivity(dictionary_activity);
        }
        menu_option_about.setOnClickListener(){
            var about_the_app = Intent(this, about_the_app::class.java)
            startActivity(about_the_app);
        }
        logout_text.setOnClickListener(){
            session.logoutUser();
            var initial_page = Intent(this, initial_page::class.java)
            startActivity(initial_page);
        }
    }
    override fun onBackPressed() {
        backToast = Toast.makeText(this, R.string.close_meesage, Toast.LENGTH_LONG)
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            finishAffinity();
            exitProcess(0)
        } else {
            backToast.show()
        }
        backPressedTime = System.currentTimeMillis()
    }
}