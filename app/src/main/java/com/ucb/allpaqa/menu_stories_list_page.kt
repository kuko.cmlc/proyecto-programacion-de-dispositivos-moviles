package com.ucb.allpaqa

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ucb.allpaqa.adapters.story.Story_adapter
import com.ucb.allpaqa.adapters.story.Story_adapter_view_2
import com.ucb.allpaqa.userLogin.UserSessionManager
import com.ucb.allpaqa.viewModels.TalesViewModel
import com.ucb.data.StoryRepository
import com.ucb.domain.Story
import com.ucb.framework.RetrofitBuilder
import com.ucb.framework.AllpaqaDataSource
import com.ucb.usecases.getAllTales
import kotlinx.android.synthetic.main.activity_menu_stories_list_page.*
import kotlin.system.exitProcess


class menu_stories_list_page : AppCompatActivity() {
    lateinit var recyclerViewListStories: RecyclerView
    private var mainviewView = TalesViewModel(
        getAllTales(
            StoryRepository(
                AllpaqaDataSource(
                    RetrofitBuilder
                )
            )
        )
    )
    private lateinit var session: UserSessionManager;
    private lateinit var talesViewModel: TalesViewModel
    private var backPressedTime:Long = 0
    lateinit var backToast: Toast
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_stories_list_page)
        session = UserSessionManager(getApplicationContext())
        setUserData();
        recyclerViewListStories = findViewById(R.id.recyclerViewListStories)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerViewListStories.layoutManager = linearLayoutManager
        menu_option_learn.setOnClickListener(){
            var introduction_page = Intent(this, options_learns_page::class.java)
            startActivity(introduction_page);
        }
        menu_option_dictionary.setOnClickListener(){
            var dictionary_activity = Intent(this, Dictionary_list::class.java)
            startActivity(dictionary_activity);
        }
        menu_option_about.setOnClickListener(){
            var about_the_app = Intent(this, about_the_app::class.java)
            startActivity(about_the_app);
        }
        logout_text.setOnClickListener(){
            session.logoutUser();
            var initial_page = Intent(this, initial_page::class.java)
            startActivity(initial_page);
        }
        mainviewView.model.observe(this, Observer(::updateUi))
        mainviewView.getAllTales();
        lienarLayoutLoading.isVisible=true;
    }

    private fun updateUi(uiModel: TalesViewModel.UiModel) {
        when (uiModel) {
            is TalesViewModel.UiModel.Content -> loadTales(uiModel.tales)
        }
    }

    private fun setUserData(){
        val user = session.userDetails
        val name = user[UserSessionManager.KEY_NAME]
        val email = user[UserSessionManager.KEY_EMAIL]
        user_name.text = email
        welcome_text_again.text = name
    }

    private fun loadTales(educationModules: List<Story>) {
        lienarLayoutLoading.isVisible=false;
        recyclerViewListStories.adapter = Story_adapter(educationModules, this)
        optionChangeView(educationModules);
    }

    private fun optionChangeView(educationModules: List<Story>){
        button_covers.setOnClickListener(){
            recyclerViewListStories.adapter = Story_adapter_view_2(educationModules, this)
        }
        button_list.setOnClickListener(){
            recyclerViewListStories.adapter = Story_adapter(educationModules, this)
        }
    }
    override fun onBackPressed() {
        backToast = Toast.makeText(this, R.string.close_meesage, Toast.LENGTH_LONG)
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            finishAffinity();
            exitProcess(0)
        } else {
            backToast.show()
        }
        backPressedTime = System.currentTimeMillis()
    }
}