package com.ucb.allpaqa.userLogin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.ucb.allpaqa.initial_page
import java.util.*

class UserSessionManager(  // Context
    var _context: Context
) {
    var pref: SharedPreferences
    var editor: SharedPreferences.Editor
    var PRIVATE_MODE = 0
    fun createUserLoginSession(name: String?, email: String?) {
        editor.putBoolean(IS_USER_LOGIN, true)
        editor.putString(KEY_NAME, name)
        editor.putString(KEY_EMAIL, email)
        editor.commit()
    }

    fun checkLogin(): Boolean {
        if (!isUserLoggedIn) {
            val i = Intent(_context, initial_page::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            _context.startActivity(i)
            return true
        }
        return false
    }
    val userDetails: HashMap<String, String?>
        get() {
            val user = HashMap<String, String?>()
            user[KEY_NAME] =
                pref.getString(KEY_NAME, null)
            user[KEY_EMAIL] =
                pref.getString(KEY_EMAIL, null)
            return user
        }

    fun logoutUser() {
        editor.clear()
        editor.commit()
        val i = Intent(_context, initial_page::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        _context.startActivity(i)
    }

    val isUserLoggedIn: Boolean
        get() = pref.getBoolean(IS_USER_LOGIN, false)

    companion object {
        private const val PREFER_NAME = "AndroidExamplePref"
        private const val IS_USER_LOGIN = "IsUserLoggedIn"
        const val KEY_NAME = "name"
        const val KEY_EMAIL = "email"
    }

    init {
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }
}